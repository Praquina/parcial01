﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    private NavMeshAgent agent;
    private GameObject target;
    
    string energy;

    public EnemyType[] enemyTypes;
    public Waves[] waves;
    [SerializeField] private GameObject buggyPrefab;
    [SerializeField] private GameObject heliPrefab;
    [SerializeField] private GameObject hoverPrefab;
    [SerializeField] private int currentiave = 0;
    [SerializeField] private int currentEnemy = 0;
    [SerializeField] private int timewaves = 0;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("PlayerBaseTarget");
        agent.SetDestination(target.transform.position);
        
    }
    // Update is called once per frame
    void Update()
    {
        energy = GameObject.Find("Energy").GetComponent<Text>().text;
        float energyF = float.Parse(energy);
        energyF += 4;
        string newEnergy = energyF.ToString();
        GameObject.Find("Energy").GetComponent<Text>().text = newEnergy;
        Destroy(this.gameObject);

    }

    public struct Waves
    {
        public Enemy[] wave;
    }
    public struct EnemyType
    {

    }

   




   
	
	

   
}
