﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour {

    public Canvas levelsCanvas;
    public Canvas menuCanvas;

	// Use this for initialization
	void Start () {
        levelsCanvas.enabled = false;
        menuCanvas.enabled = true;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LevelButton()
    {
        levelsCanvas.enabled = true;
        menuCanvas.enabled = false;

    }

    public void Quit()
    {
        Application.Quit(); 
    }

    public void Back()
    {
        levelsCanvas.enabled = false;
        menuCanvas.enabled = true;
    }
}
