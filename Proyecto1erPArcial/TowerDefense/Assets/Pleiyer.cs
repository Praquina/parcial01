﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pleiyer : MonoBehaviour {

    

    [SerializeField] private GameObject machineGunPrefab;
    [SerializeField] private GameObject missilPrefab;
    [SerializeField] private GameObject sniperPrefab;
    [SerializeField] private GameObject holdingWeapon;

    string energyText;

   public void CreateWeapon(string boton)
        {
        if (boton == "machinegun")
        {
            holdingWeapon = Instantiate(machineGunPrefab, Input.mousePosition, Quaternion.identity);

        }
        if (boton == "missile")
        {
          //  holdingWeapon = Instantiate(machineGunPrefab, Input.mousePosition, Quaternion.identity);

        }
        if (boton == "sniper")
        {
           // holdingWeapon = Instantiate(machineGunPrefab, Input.mousePosition, Quaternion.identity);

        }
    }

    // Update is called once per frame
    void Update () {
        if (float.Parse(GameObject.Find("MoneyText").GetComponent<Text>().text) >= 4)
        {
            energyText = GameObject.Find("MoneyText").GetComponent<Text>().text;

            if (holdingWeapon != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    holdingWeapon.transform.position = hitInfo.point;
                    if (Input.GetMouseButton(0))
                    {
                        if (hitInfo.transform.CompareTag("Placement"))
                        {

                       
                            holdingWeapon = null;
                            Destroy(hitInfo.collider);

                            float energyFloat = float.Parse(energyText);
                            energyFloat = energyFloat - 4;
                            string newEnergy = energyFloat.ToString();
                            GameObject.Find("MoneyText").GetComponent<Text>().text = newEnergy;
                        }
                    }
                }
            }
        }

        
		
	}
    public void InstantiateWeapon(string weapon)
    {
        if (weapon == "machineGun" && float.Parse(GameObject.Find("MoneyText").GetComponent<Text>().text) >= 4)
        {
            holdingWeapon = Instantiate(machineGunPrefab, Input.mousePosition, Quaternion.identity);
        }
    }
}
