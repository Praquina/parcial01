﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camerea : MonoBehaviour {

    [SerializeField] private Vector2 mousePos;
    [SerializeField] private float screenwidth;
    [SerializeField] private float screenHeight;
    [SerializeField] private float cameraMoveSpeed = 5f;

    // Use this for initialization
    void Update () {
        MoveCamera();
	}
	
	// Update is called once per frame
	void MoveCamera () {
        mousePos = Input.mousePosition;
        screenwidth = Screen.width;
        screenHeight = Screen.height;

        Vector3 cameraPos = transform.position;

        if (mousePos.x < (screenwidth * .10f) && cameraPos.z < 10f)
        {
            if (cameraPos.z < 10)
            {
                cameraPos.z += Time.deltaTime * cameraMoveSpeed;
            }
            
        }

        if (mousePos.y < (screenHeight * .10f) && cameraPos.x < 10f)
        {
            if (cameraPos.x  >-27)
            {
                cameraPos.x -= Time.deltaTime * cameraMoveSpeed;
            }
            
        }

        if (mousePos.x > (screenwidth * .90f) && cameraPos.z < 90f)
        {
            if (cameraPos.z > -2)
            {
                cameraPos.z -= Time.deltaTime * cameraMoveSpeed;
            }
            
        }

        if (mousePos.y > (screenHeight * .90f) && cameraPos.x < 90f)
        {
            if (cameraPos.x < 10)
            {
                cameraPos.x += Time.deltaTime * cameraMoveSpeed;
            }
        }

        transform.position = cameraPos;



    }


}
