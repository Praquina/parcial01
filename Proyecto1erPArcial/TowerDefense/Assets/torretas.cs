﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class torretas : MonoBehaviour {

    private GameObject closestEnemy;

    [SerializeField] GameObject targetEnemy;
    private GameObject[] enemies;
    public int distance = 100;

    buggie healthEn;


	// Use this for initialization
	private void Start () {
        enemies = GameObject.FindGameObjectsWithTag("Enemies");
	}
	
	// Update is called once per frame
	void Update () {
        enemies = GameObject.FindGameObjectsWithTag("Enemies");
        for (int i = 0; i < enemies.Length; i++)
        {
            if (Vector3.Distance(enemies[i].transform.position, this.transform.position) < distance)
            {
                if (targetEnemy == null)
                {
                    targetEnemy = enemies[i];
                    StartCoroutine(Shoot());

                }
                transform.LookAt(targetEnemy.transform);
                if (Vector3.Distance(targetEnemy.transform.position, this.transform.position) >= distance)
                {
                    targetEnemy = null;
                    StopAllCoroutines();
                }
            }
        }
	}
    public IEnumerator Shoot()
    {
        while(targetEnemy != null)
        {
            healthEn = targetEnemy.GetComponent<buggie>();
            //print(healthEn.enemyLife);
            healthEn.enemyLife -= 5;
            print(healthEn.enemyLife);
            yield return new WaitForSeconds(1.0f);

        }
    }
}
