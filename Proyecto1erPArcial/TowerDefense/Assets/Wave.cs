﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;



public class Wave : MonoBehaviour {
    
    public enum EnemyType
    {
        buggy,
        helicopter,
        cover,
    };

    [Serializable] 
    public struct Waves
    {
        public EnemyType[] wave;
    }

    public Waves[] waves;
    

    [SerializeField] private GameObject buggyPrefab;
    [SerializeField] private int currentWave = 0;
    [SerializeField] private int currentEnemy = 0;
    [SerializeField] private int TimeBetweenWaves = 5;


    public IEnumerator SendWave()
    {

        while (currentWave < waves.Length) 
            {
            while (currentEnemy < waves[currentWave].wave.Length)
            {
                switch (waves[currentWave].wave[currentEnemy])
                {
                    case EnemyType.buggy:
                        Instantiate(buggyPrefab, this.transform.position, Quaternion.identity);
                        // yield return new WaitForSeconds(3);
                        break;
                }

                yield return new WaitForSeconds(3);
                currentEnemy++;
            }
                
          

            currentWave++;
            currentEnemy = 0;
            yield return new WaitForSeconds(TimeBetweenWaves);
        }
            
       
           
     }
    public void Wabe()
    {

        StartCoroutine(SendWave());
    }

}
