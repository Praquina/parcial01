﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class buggie : MonoBehaviour {

   private NavMeshAgent agent;
    private GameObject target;
    public int enemyLife;
    public float life;
    string HealthUI;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("PlayerBaseTarget");
        agent.SetDestination(target.transform.position);
        enemyLife = 10;

    }
	
	// Update is called once per frame
	void Update () {
        if (enemyLife <= 0)
        {
            Destroy(this.gameObject);
        }
	}

    public void CarroLife(int damage)
    {
        life = life - damage;
        if (life == 0)
        {
            Destroy(this.gameObject, 1);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Cubo")
        {
            HealthUI = GameObject.Find("vida").GetComponent<Text>().text;
            float healthf = float.Parse(HealthUI);
            healthf -= 1;
            string newHealth = healthf.ToString();
            GameObject.Find("vida").GetComponent<Text>().text = newHealth;
            Destroy(gameObject, 1);
        }
    }
}
